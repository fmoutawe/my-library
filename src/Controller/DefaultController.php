<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Volume;
use App\Form\BookType;
use App\Form\VolumeType;
use App\Repository\VolumeRepository;
use App\Twig\AppExtension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{
    public function index(Request $request)
    {
        $appExtension = $this->get(AppExtension::class);

        $em = $this->getDoctrine()->getManager();
        $book = new Book();
        $form = $this
            ->createForm(BookType::class, $book);

        $form->handleRequest($request);

        $config = [];

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($book);
            $em->flush();

            $id = $appExtension->toHashIdFilter($book->getName());

            return $this->redirect($this->generateUrl('index') . '#' . $id);
        }

        $config['form'] = $form->createView();
        $config['books'] = $em->getRepository(Book::class)->findBy([], ['name' => 'ASC']);

        /** @var VolumeRepository $volumeRespository */
        $volumeRespository = $em->getRepository(Volume::class);
        $config['countVolume'] = $volumeRespository->count([]);

        foreach ($config['books'] as $index => $book) {
            $volume = (new Volume())->setBook($book);
            $form2 = $this->get('form.factory')->createNamed('volume' . $index, VolumeType::class, $volume);
            $book->form = $form2->createView();

            $form2->handleRequest($request);

            if ($form2->isSubmitted() && $form2->isValid()) {
                $em->persist($volume);
                $em->flush();

                $id = $appExtension->toHashIdFilter($book->getName());

                return $this->redirect($this->generateUrl('index') . '#' . $id);
            }
        }

        return $this->render('default/index.html.twig', $config);
    }

    public function book(Book $book, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $volume = new Volume();
        $volume->setBook($book);
        $form = $this
            ->createForm(VolumeType::class, $volume);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($volume);
            $em->flush();

            return $this->redirect($this->generateUrl('book', ['id' => $book->getId()]));
        }

        return $this->render('default/book.html.twig', ['book' => $book, 'form' => $form->createView()]);
    }

    public function volumeRemove(Volume $volume) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($volume);
        $em->flush();

        return $this->redirect($this->generateUrl('book', ['id' => $volume->getBook()->getId()]));
    }

    public function remove(Book $book)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($book);
        $em->flush();

        return $this->redirect($this->generateUrl('index'));
    }
}