<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('toHashId', array($this, 'toHashIdFilter')),
        );
    }

    public function toHashIdFilter($label)
    {
        return md5($label);
    }
}